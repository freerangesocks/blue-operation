##############################################
#     Basic_Linux_Endpoint_Audit.sh          #
# This script should work on most windows    #
#            machines                        #
#  SOCKS created this 10/04/2023 with the    # 
# Intent of basic info gathering on windows  #
# Machines ... feel free to modify for use   #
##############################################


#!/bin/bash

if [[ $EUID -ne 0 ]]; then
    echo "You must run this script as root."
    exit 1
fi

echo "Select OS:"
echo "1) YUM-based (e.g., CentOS, RHEL)"
echo "2) Debian-based (e.g., Ubuntu, Debian)"
echo "3) Pacman-based (e.g., Arch, Manjaro)"
read -p "Enter your choice (1/2/3): " os_choice

echo "Is this a:"
echo "1) Server"
echo "2) Endpoint"
read -p "Enter your choice (1/2): " machine_choice

echo "Is this a:"
echo "1) Standard Collection"
echo "2) Incident Response Mode"
read -p "Enter your choice (1/2): " mode_choice

read -p "Enter the path where you want to save the information (or leave blank for current directory): " save_path

if [[ -z "$save_path" ]]; then
    save_path="./"
else
    save_path="${save_path}/"
fi

timestamp=$(date "+%Y-%m-%d %H:%M:%S")
output_file="${save_path}system_info_${timestamp}.txt"

if [[ $mode_choice -eq 2 ]]; then
    log_date=$(date -d "7 days ago" "+%b %e")
else
    log_date=$(date -d "yesterday" "+%b %e")
fi

# Start collecting data with a timestamp
echo "Data collection started at: $timestamp" >> "$output_file"
echo "========================================" >> "$output_file"

# Hardware Details
echo "=== Hardware Details ===" >> "$output_file"
lscpu >> "$output_file"
echo "----------------------" >> "$output_file"
lsmem >> "$output_file"
echo "----------------------" >> "$output_file"
lsblk >> "$output_file"
echo "----------------------" >> "$output_file"
lspci >> "$output_file"
echo "----------------------" >> "$output_file"

# Users & Authentication
echo "=== Users & Authentication ===" >> "$output_file"
cat /etc/passwd >> "$output_file"
cat /etc/group >> "$output_file"
cat /etc/shadow >> "$output_file"
cat /etc/sudoers >> "$output_file"
cat /etc/sudoers.d/* >> "$output_file" 2>/dev/null
who >> "$output_file"
last >> "$output_file"

echo "=== Passwordless Accounts ===" >> "$output_file"
awk -F: '($2 == "" ) { print $1 }' /etc/shadow >> "$output_file"

# File System & Permissions
echo "World-writable files (potential security risk):" >> "$output_file"
find / -type f -perm -002 ! -path "/proc/*" ! -path "/sys/*" 2>/dev/null >> "$output_file"
echo "SetUID and SetGID files (potential for privilege escalation):" >> "$output_file"
find / -type f \( -perm -4000 -o -perm -2000 \) ! -path "/proc/*" ! -path "/sys/*" 2>/dev/null >> "$output_file"

echo "=== File System & Permissions ===" >> "$output_file"
cat /etc/fstab >> "$output_file"
mount >> "$output_file"
df -h >> "$output_file"
du -sh /* >> "$output_file" 2>/dev/null

# Processes & Services
echo "=== Processes & Services ===" >> "$output_file"
ps aux >> "$output_file"

echo "Top 5 Processes by Utilization:" >> "$output_file"
ps aux --sort=-%cpu | head -6 >> "$output_file"

echo "Active System Services:" >> "$output_file"
systemctl list-unit-files --type=service >> "$output_file"
netstat -tuln >> "$output_file" 2>/dev/null
ss -tuln >> "$output_file" 2>/dev/null

# Crontab entries
echo "=== Crontab Entries ===" >> "$output_file"
for user in $(cut -f1 -d: /etc/passwd); do
    echo "Crontab for $user:" >> "$output_file"
    crontab -l -u $user >> "$output_file" 2>/dev/null
    echo "----------------------" >> "$output_file"
done

# Network Configuration
echo "=== Network Configuration ===" >> "$output_file"
ip addr >> "$output_file"
ip route >> "$output_file"
iptables -L -vn >> "$output_file" 2>/dev/null

# Software & Packages
echo "=== Software & Packages ===" >> "$output_file"
case $os_choice in
    1) 
        rpm -qa >> "$output_file"
        cat /etc/yum.repos.d/* >> "$output_file" 2>/dev/null
        echo "Available updates:" >> "$output_file"
        yum check-update >> "$output_file"
        ;;
    2) 
        dpkg -l >> "$output_file"
        cat /etc/apt/sources.list >> "$output_file"
        apt-get update > /dev/null && apt-get upgrade -s >> "$output_file"
        ;;
    3) 
        pacman -Q >> "$output_file"
        echo "For pacman-based systems, manually check for updates." >> "$output_file"
        ;;
esac

# Logging & Monitoring
echo "=== Logging & Monitoring ===" >> "$output_file"
echo "-- Log Configurations --" >> "$output_file"
echo "Logrotate Main Configuration:" >> "$output_file"
cat /etc/logrotate.conf >> "$output_file"
echo "Logrotate Additional Configurations:" >> "$output_file"
cat /etc/logrotate.d/* >> "$output_file" 2>/dev/null
echo "Audit Daemon Configuration:" >> "$output_file"
cat /etc/audit/auditd.conf >> "$output_file" 2>/dev/null
echo "Audit Rules:" >> "$output_file"
cat /etc/audit/rules.d/* >> "$output_file" 2>/dev/null

echo "-- Last Day's Logs --" >> "$output_file"
journalctl --since="$log_date" >> "$output_file" 2>/dev/null

# Memory Dump in IR Mode
if [[ $mode_choice -eq 2 ]]; then
    echo "=== Memory Dump ===" >> "$output_file"
    if command -v dumpcap > /dev/null; then
        dumpcap -w "${save_path}memory_dump_${timestamp}.pcap"
        echo "Memory dump saved to ${save_path}memory_dump_${timestamp}.pcap" >> "$output_file"
    else
        echo "Dumpcap not found. No memory dump captured." >> "$output_file"
    fi
fi

# Kernel Parameters
echo "=== Kernel Parameters ===" >> "$output_file"
sysctl -a | egrep "kern|boot|vm|fs|proc" >> "$output_file"

# Conclude
echo "========================================" >> "$output_file"
echo "Data collection ended at: $(date "+%Y-%m-%d %H:%M:%S")" >> "$output_file"

echo "Data collected and saved to $output_file"
