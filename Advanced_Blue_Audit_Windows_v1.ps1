##############################################
#      Advanced  Blue Audit v1.0             #
# This script should work on most windows    #
#            machines                        #
#  SOCKS created this 10/04/2023 with the    # 
# Intent of basic info gathering on windows  #
# Machines ... feel free to modify for use   #
##############################################

# Common Functions
function GatherSystemInfo {
    wmic computersystem get Caption,Manufacturer,Model,Name,NumberOfProcessors,TotalPhysicalMemory,SystemType,UserName
}

function NetworkInfo {
    wmic nicconfig where "IPEnabled='TRUE'" get Caption,DefaultIPGateway,Description,DHCPServer,IPAddress,IPSubnet,MACAddress
}

function MonitorProcesses {
    Get-Process | Format-Table Name, CPU, Id
}

function MonitorServices {
    Get-Service | Where-Object { $_.Status -eq "Running" } | Format-Table DisplayName, Status
}

# Security Check Functions
function ListFirewallRules {
    Get-NetFirewallRule | Where-Object { $_.Enabled -eq 'True' -and $_.Direction -eq 'Inbound' } | Format-Table Name, DisplayName, Action
}

function CheckLocalAdmins {
    net localgroup administrators
}

function CheckStartupLocations {
    Write-Host "Checking registry startup locations..."
    $regLocations = @(
        'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Run',
        'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce',
        'HKCU:\Software\Microsoft\Windows\CurrentVersion\Run',
        'HKCU:\Software\Microsoft\Windows\CurrentVersion\RunOnce'
    )

    foreach ($location in $regLocations) {
        Write-Host "Checking $location"
        Get-ItemProperty -Path $location | Select-Object -Property * -ExcludeProperty PS*
    }

    Write-Host "Checking file system startup locations..."
    $fileLocations = @(
        'C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp',
        "$env:APPDATA\Microsoft\Windows\Start Menu\Programs\Startup"
    )

    foreach ($location in $fileLocations) {
        Write-Host "Checking $location"
        Get-ChildItem -Path $location
    }
}

function CheckRemoteConnections {
    $activeConnections = netstat -an | where-object { $_ -match '^(TCP)' }
    $sshConnections = $activeConnections | where-object { $_ -match ':22\s' }
    $rdpConnections = $activeConnections | where-object { $_ -match ':3389\s' }
    $vncConnections = $activeConnections | where-object { $_ -match ':59[0-9]{2}\s' }

    if ($sshConnections) {
        Write-Host "Detected SSH connections:"
        $sshConnections
    } else {
        Write-Host "No active SSH connections found."
    }

    if ($rdpConnections) {
        Write-Host "Detected RDP connections:"
        $rdpConnections
    } else {
        Write-Host "No active RDP connections found."
    }

    if ($vncConnections) {
        Write-Host "Detected VNC connections:"
        $vncConnections
    } else {
        Write-Host "No active VNC connections found."
    }
}

# Domain-specific Functions
function EnumerateDomainUsers {
    dsquery user
}

function EnumerateDomainAdmins {
    net group "Domain Admins" /domain
}

function EnumerateLockedOutAccounts {
    Search-ADAccount -LockedOut
}

function EnumerateDisabledAccounts {
    dsquery user -disabled
}

function UsersPasswordNeverExpire {
    Search-ADAccount -PasswordNeverExpires | Where-Object { $_.ObjectClass -eq 'user' } | Format-Table Name, ObjectClass, PasswordLastSet
}

# Check if the machine is domain-joined
function IsDomainJoined {
    $computerInfo = Get-WmiObject Win32_ComputerSystem
    return $computerInfo.PartOfDomain
}

# Main user prompt function
function UserPrompt {
    do {
        if (IsDomainJoined) {
            Write-Host "Domain-Joined Endpoint Options:"
            Write-Host "1. Gather System Info"
            Write-Host "2. Network Info"
            Write-Host "3. Monitor Processes"
            Write-Host "4. Monitor Services"
            Write-Host "5. List Firewall Rules"
            Write-Host "6. Check Local Admins"
            Write-Host "7. Check Startup Locations"
            Write-Host "8. Check Remote Connections"
            Write-Host "9. Enumerate Domain Users"
            Write-Host "10. Enumerate Domain Admins"
            Write-Host "11. Enumerate Locked Out Accounts"
            Write-Host "12. Enumerate Disabled Accounts"
            Write-Host "13. Users Whose Passwords Never Expire"
            Write-Host "14. Exit"
        } else {
            Write-Host "Standalone Endpoint Options:"
            Write-Host "1. Gather System Info"
            Write-Host "2. Network Info"
            Write-Host "3. Monitor Processes"
            Write-Host "4. Monitor Services"
            Write-Host "5. List Firewall Rules"
            Write-Host "6. Check Local Admins"
            Write-Host "7. Check Startup Locations"
            Write-Host "8. Check Remote Connections"
            Write-Host "9. Exit"
        }

        $choice = Read-Host "Welcome to the Sockdrawer Enter your choice"
        
        switch($choice) {
            1 { GatherSystemInfo }
            2 { NetworkInfo }
            3 { MonitorProcesses }
            4 { MonitorServices }
            5 { ListFirewallRules }
            6 { CheckLocalAdmins }
            7 { CheckStartupLocations }
            8 { CheckRemoteConnections }
            9 {
                if (IsDomainJoined) {
                    EnumerateDomainUsers
                } else {
                    return
                }
            }
            10 { EnumerateDomainAdmins }
            11 { EnumerateLockedOutAccounts }
            12 { EnumerateDisabledAccounts }
            13 { UsersPasswordNeverExpire }
            14 { return }
            default { Write-Host "Invalid Choice!" }
        }
    } while ($true)
}

# Start the user prompt
UserPrompt
