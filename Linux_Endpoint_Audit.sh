#!/bin/bash

echo "Select OS:"
echo "1) YUM-based (e.g., CentOS, RHEL)"
echo "2) Debian-based (e.g., Ubuntu, Debian)"
echo "3) Pacman-based (e.g., Arch, Manjaro)"
read -p "Enter your choice (1/2/3): " os_choice

echo "Is this a:"
echo "1) Server"
echo "2) Endpoint"
read -p "Enter your choice (1/2): " machine_choice

read -p "Enter the path where you want to save the information (or leave blank for current directory): " save_path

if [[ -z "$save_path" ]]; then
    save_path="./"
else
    save_path="${save_path}/"
fi

timestamp=$(date "+%Y-%m-%d %H:%M:%S")
output_file="${save_path}system_info_${timestamp}.txt"

# Start collecting data with a timestamp
echo "Data collection started at: $timestamp" >> "$output_file"
echo "========================================" >> "$output_file"

# Hardware Details
echo "=== Hardware Details ===" >> "$output_file"
lscpu >> "$output_file"
echo "----------------------" >> "$output_file"
lsmem >> "$output_file"
echo "----------------------" >> "$output_file"
lsblk >> "$output_file"
echo "----------------------" >> "$output_file"
lspci >> "$output_file"
echo "----------------------" >> "$output_file"

# Users & Authentication
echo "=== Users & Authentication ===" >> "$output_file"
cat /etc/passwd >> "$output_file"
cat /etc/group >> "$output_file"
cat /etc/shadow >> "$output_file"  # Ensure you have proper permissions!
cat /etc/sudoers >> "$output_file"
cat /etc/sudoers.d/* >> "$output_file" 2>/dev/null
who >> "$output_file"
last >> "$output_file"

# File System & Permissions

echo "World-writable files (potential security risk):" >> "$output_file"
find / -type f -perm -0022 ! -path "/proc/*" ! -path "/sys/*" 2>/dev/null >> "$output_file"
echo "SetUID and SetGID files (potential for privilege escalation):" >> "$output_file"
find / -type f \( -perm -4000 -o -perm -2000 \) ! -path "/proc/*" ! -path "/sys/*" 2>/dev/null >> "$output_file"


echo "=== File System & Permissions ===" >> "$output_file"
find / -type f \( -perm -2 ! -type l \) >> "$output_file" # world-writable files
find / -nouser >> "$output_file" # no owner files
find / -perm /6000 >> "$output_file" # SUID/SGID files
cat /etc/fstab >> "$output_file"
mount >> "$output_file"
df -h >> "$output_file"
du -sh /* >> "$output_file" 2>/dev/null # Checking root directories as example

# Processes & Services
echo "=== Processes & Services ===" >> "$output_file"
ps aux >> "$output_file"

echo "Top 5 Processes by Utilization:" >> "$output_file"
ps aux --sort=-%cpu | head -6 >> "$output_file"

echo "Active System Services:" >> "$output_file"
systemctl list-unit-files --type=service >> "$output_file"
netstat -tuln >> "$output_file" 2>/dev/null
ss -tuln >> "$output_file" 2>/dev/null

# Crontab entries
echo "=== Crontab Entries ===" >> "$output_file"
for user in $(cut -f1 -d: /etc/passwd); do
    echo "Crontab for $user:" >> "$output_file"
    crontab -l -u $user >> "$output_file" 2>/dev/null
    echo "----------------------" >> "$output_file"
done

# Network Configuration
echo "=== Network Configuration ===" >> "$output_file"
ip addr >> "$output_file"
ip route >> "$output_file"
iptables -L -vn >> "$output_file" 2>/dev/null

# Software & Packages
echo "=== Software & Packages ===" >> "$output_file"
case $os_choice in
    1) 
        rpm -qa >> "$output_file"
        cat /etc/yum.repos.d/* >> "$output_file" 2>/dev/null
        ;;
    2) 
        dpkg -l >> "$output_file"
        cat /etc/apt/sources.list >> "$output_file"
        ;;
    3) pacman -Q >> "$output_file" ;;
esac

# Logging & Monitoring
echo "=== Logging & Monitoring ===" >> "$output_file"

# Extract log configurations
echo "-- Log Configurations --" >> "$output_file"
echo "Logrotate Main Configuration:" >> "$output_file"
cat /etc/logrotate.conf >> "$output_file"
echo "----------------------" >> "$output_file"
echo "Logrotate Additional Configurations:" >> "$output_file"
cat /etc/logrotate.d/* >> "$output_file" 2>/dev/null
echo "----------------------" >> "$output_file"
echo "Audit Daemon Configuration:" >> "$output_file"
cat /etc/audit/auditd.conf >> "$output_file" 2>/dev/null
echo "----------------------" >> "$output_file"
echo "Audit Rules:" >> "$output_file"
cat /etc/audit/rules.d/* >> "$output_file" 2>/dev/null
echo "----------------------" >> "$output_file"

# Extract log entries and metadata
for log in /var/log/*; do
    if [ -f "$log" ]; then
        echo "Log File: $log" >> "$output_file"
        
        # Capture file size
        echo "Size:" >> "$output_file"
        du -sh "$log" >> "$output_file"
          
        # Capture the earliest entry from the past 24 hours
        echo "Earliest Entry from the last 24 hours:" >> "$output_file"
        grep -m 1 "$(date -d "yesterday" "+%b %e")" "$log"
        # Capture the earliest entry from the past 24 hours
        echo "Earliest Entry from the last 24 hours:" >> "$output_file"
        grep -m 1 "$(date -d "yesterday" "+%b %e")" "$log" >> "$output_file" # Assumes logs have standard 'Month Day' format
        
        echo "----------------------" >> "$output_file"
    fi
done

# Security Configuration
echo "=== Security Configuration ===" >> "$output_file"
sestatus >> "$output_file" 2>/dev/null
apparmor_status >> "$output_file" 2>/dev/null
cat /etc/pam.d/* >> "$output_file" 2>/dev/null

# Backup & Recovery
echo "=== Backup & Recovery ===" >> "$output_file"
if command -v rsync > /dev/null; then
    echo "Rsync is installed." >> "$output_file"
    
else
    echo "Rsync is not installed." >> "$output_file"
fi

# Print completion timestamp
echo "========================================" >> "$output_file"
echo "Data collection completed at: $(date "+%Y-%m-%d %H:%M:%S")" >> "$output_file"

echo "Information saved to $output_file"
