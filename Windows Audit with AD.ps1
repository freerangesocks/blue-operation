<#
.SYNOPSIS
An Active Directory & Windows Server audit script. 
Should Be Compatible with Windows 7, Windows Server 2008 or newer. Powershell version 2.0+
.DESCRIPTION
Version 1.0 - 03-OCT-2023 Created by Nico 'Socks' Smith
.EXAMPLE
.\Invoke-Audit.ps1 -Verbose
.\Invoke-Audit.ps1 -AD -Verbose
.PARAMETER AD
If you use the -AD Switch, the script will include Active Directory specific commands.
#>
[CmdletBinding()]
param
(
    [switch]$AD
)

$Computername = $env:COMPUTERNAME
$Path = "Audit-$Computername"

# Ensure running as admin
$isAdmin = ([System.Security.Principal.WindowsPrincipal][System.Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)
if (-not $isAdmin) {
    throw "Commands require administrator privileges to execute!"
}

# Ensure compatible PS Version
if ($PSVersionTable.PSVersion.Major -lt 2) {
    throw "This script is only compatible with Powershell Version 2.0+"
}

# Ensure or create output directory
if (-not (Test-Path -Path $Path)) {
    New-Item -Path $Path -ItemType Directory -ErrorAction Stop | Out-Null
} else {
    $r = Read-Host -Prompt "Audit Folder already exists. Continue & Overwrite? [Y/N]"
    if ($r -notmatch "^[Yy]") {
        Write-Verbose "Exiting script"
        exit
    }
}

Set-Location -Path $Path

# System Information
$SystemRoles = @{
    0 = "Standalone Workstation"
    1 = "Member Workstation"
    2 = "Standalone Server"
    3 = "Member Server"
    4 = "Backup  Domain Controller"
    5 = "Primary Domain Controller"
}

$ComputerRole = $SystemRoles[[int]((Get-WMIObject -Class Win32_ComputerSystem).DomainRole)]

$OS = Get-WMIObject -class Win32_OperatingSystem

$SysInfo = @{
    Date          = Get-Date -UFormat "%d-%m-%y"
    CurrentUser   = $env:UserName
    CurrentDomain = $env:UserDomain
    Hostname      = $env:ComputerName
    OS            = $OS.Caption
    ServicePack   = $OS.ServicePackMajorVersion
    PSVersion     = $PSVersionTable.PSVersion.Major
    Role          = $ComputerRole
}

$SysInfo.GetEnumerator() | ForEach-Object {
    Write-Verbose "$($_.Key): $($_.Value)"
    "$($_.Key): $($_.Value)" | Out-File -FilePath "Sysinfo.txt" -Append
}

# Collect Local System & User Information
SecEdit.exe /export /cfg SecPolicy.txt /quiet
w32tm /query /status | Out-File -FilePath "NTPConfig.txt"
auditpol.exe /get /category:* | Out-File -FilePath "AuditPolicy.txt"
[System.IO.DriveInfo]::GetDrives() | Export-CSV -Path "LocalDrives.csv" -Delimiter ';' -NoTypeInformation
netsh advfirewall show allprofiles | Out-File -FilePath "Firewall_Profiles.txt"
netsh advfirewall firewall show rule all verbose | Out-File -FilePath "Firewall_Rules.txt"

# Active Directory Section
if ($AD) {
    try {
        Import-Module ActiveDirectory -ErrorAction Stop

    } catch {
        Write-Error "Error importing Active Directory module: $_"
    }
}

# Complete
Write-Verbose "Script Complete"
