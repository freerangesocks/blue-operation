#!/bin/bash

# Function to install BIND9
install_bind9() {
    echo "Updating package lists..."
    sudo apt-get update
    echo "Installing BIND9 and related packages..."
    sudo apt-get install bind9 bind9utils bind9-doc
}

# Function to configure BIND for IPv4
configure_ipv4() {
    echo "Configuring BIND9 for IPv4 mode..."
    sudo sed -i 's/OPTIONS.*/OPTIONS="-4 -u bind"/' /etc/default/bind9
}

# Function to configure primary DNS server
configure_primary_dns() {
    local zone_name=$1
    local reverse_zone_name=$2
    local primary_ip=$3

    # Configure options file
    echo "Configuring options file..."
    sudo bash -c "cat > /etc/bind/named.conf.options << EOF
acl \"trusted\" {
    $primary_ip;
};
options {
    directory \"/var/cache/bind\";
    recursion yes;
    allow-recursion { trusted; };
    listen-on { $primary_ip; };
    allow-transfer { none; };
    forwarders {
        8.8.8.8;
        8.8.4.4;
    };
};
EOF"

    # Configure local file
    echo "Configuring local file..."
    sudo bash -c "cat > /etc/bind/named.conf.local << EOF
zone \"$zone_name\" {
    type master;
    file \"/etc/bind/zones/db.$zone_name\";
    allow-transfer { none; };
};
zone \"$reverse_zone_name\" {
    type master;
    file \"/etc/bind/zones/db.$reverse_zone_name\";
    allow-transfer { none; };
};
EOF"
}

# Main script starts here

echo "Starting BIND9 installation and configuration script..."

# Prompt user for zone names and IP addresses
read -p "Enter the domain (e.g., nyc3.example.com): " domain
read -p "Enter the reverse zone name (e.g., 128.10.in-addr.arpa): " reverse_zone
read -p "Enter the primary DNS server IP address: " primary_ip

install_bind9
configure_ipv4
configure_primary_dns "$domain" "$reverse_zone" "$primary_ip"

echo "BIND9 installation and basic configuration completed."
echo "Please proceed to manually configure forward and reverse zone files, secondary DNS server, and DNS clients as needed."
