##############################################
#          Basic Blue Audit v1.0             #
# This script should work on most windows    #
#            machines                        #
#  SOCKS created this 10/04/2023 with the    # 
# Intent of basic info gathering on windows  #
# Machines ... feel free to modify for use   #
##############################################

# Functions
function IsDomainJoined {
    $status = (Get-WmiObject -Class Win32_ComputerSystem).PartOfDomain
    return $status
}

function ListFirewallRules {
    Get-NetFirewallRule | Where-Object { $_.Enabled -eq 'True' -and $_.Direction -eq 'Inbound' } | Format-Table Name, DisplayName, Action
}

function CheckLocalAdmins {
    net localgroup administrators
}

function CheckActiveProcesses {
    Get-Process | Format-Table Name, Id, MainWindowTitle
}

function CheckStartupLocations {
    Write-Host "Checking registry startup locations..."
    $regLocations = @(
        'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Run',
        'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce',
        'HKCU:\Software\Microsoft\Windows\CurrentVersion\Run',
        'HKCU:\Software\Microsoft\Windows\CurrentVersion\RunOnce'
    )

    foreach ($location in $regLocations) {
        Write-Host "Checking $location"
        Get-ItemProperty -Path $location | Select-Object -Property * -ExcludeProperty PS*
    }

    Write-Host "Checking file system startup locations..."
    $fileLocations = @(
        'C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp',
        "$env:APPDATA\Microsoft\Windows\Start Menu\Programs\Startup"
    )

    foreach ($location in $fileLocations) {
        Write-Host "Checking $location"
        Get-ChildItem -Path $location
    }
}

function CheckRemoteConnections {
    $activeConnections = netstat -an | where-object { $_ -match '^(TCP)' }

    $sshConnections = $activeConnections | where-object { $_ -match ':22\s' }
    $rdpConnections = $activeConnections | where-object { $_ -match ':3389\s' }
    $vncConnections = $activeConnections | where-object { $_ -match ':59[0-9]{2}\s' }

    if ($sshConnections) {
        Write-Host "Detected SSH connections:"
        $sshConnections
    } else {
        Write-Host "No active SSH connections found."
    }

    if ($rdpConnections) {
        Write-Host "Detected RDP connections:"
        $rdpConnections
    } else {
        Write-Host "No active RDP connections found."
    }

    if ($vncConnections) {
        Write-Host "Detected VNC connections:"
        $vncConnections
    } else {
        Write-Host "No active VNC connections found."
    }
}

function CheckRDPConnections {
    $rdpLogons = Get-WinEvent -LogName Security | Where-Object { $_.Id -eq 4624 -and $_.Properties[8].Value -eq 10 }
    foreach ($log in $rdpLogons) {
        $username = $log.Properties[5].Value
        $ipAddress = $log.Properties[18].Value
        Write-Host "User $username connected via RDP from IP $ipAddress"
    }
}

function CheckFailedWindowsLogons {
    $failedLogons = Get-WinEvent -LogName Security -MaxEvents 10 | Where-Object { $_.Id -eq 4625 }
    foreach ($log in $failedLogons) {
        $username = $log.Properties[5].Value
        $status = $log.Properties[8].Value
        Write-Host "Failed logon attempt for user $username with status $status"
    }
}

function CheckSSHConnections {
    $sshdLogs = Get-WinEvent -LogName Application -MaxEvents 20 | Where-Object { $_.ProviderName -eq "sshd" }
    $successfulSSH = $sshdLogs | Where-Object { $_.Id -eq 703 } | Select-Object -First 10
    foreach ($log in $successfulSSH) {
        $details = $log.Message
        Write-Host "Successful SSH Connection: $details"
    }
    
    $failedSSH = $sshdLogs | Where-Object { $_.Id -eq 702 } | Select-Object -First 10
    foreach ($log in $failedSSH) {
        $details = $log.Message
        Write-Host "Failed SSH Connection: $details"
    }
}

function DisplayMenu {
    if (IsDomainJoined) {
        Write-Host "This is a domain-joined machine."
        Write-Host "1. Check Firewall Rules"
        Write-Host "2. Check Local Administrators Group"
        Write-Host "3. Check Active Processes"
        Write-Host "4. Check Startup Locations"
        Write-Host "5. Check Remote Connections"
        Write-Host "6. Check RDP Connections"
        Write-Host "7. Check Failed Windows Logons"
        Write-Host "8. Check SSH Connections"
    } else {
        Write-Host "This is a standalone machine."
        Write-Host "1. Check Firewall Rules"
        Write-Host "2. Check Local Administrators Group"
        Write-Host "3. Check Active Processes"
        Write-Host "4. Check Startup Locations"
        Write-Host "5. Check Remote Connections"
    }

    $selection = Read-Host "Enter the number of the desired check"
    return $selection
}

# Script Execution
do {
    $choice = DisplayMenu

    switch ($choice) {
        '1' { ListFirewallRules }
        '2' { CheckLocalAdmins }
        '3' { CheckActiveProcesses }
        '4' { CheckStartupLocations }
        '5' { CheckRemoteConnections }
        '6' { if (IsDomainJoined) { CheckRDPConnections } }
        '7' { if (IsDomainJoined) { CheckFailedWindowsLogons } }
        '8' { if (IsDomainJoined) { CheckSSHConnections } }
        default { Write-Host "Invalid selection." }
    }

    $repeat = Read-Host "Do you want to perform another check? (yes/no)"
} while ($repeat -eq "yes")
