# Define output file
$file = "compliance_and_system_report.txt"

# Output basic system info to the file
$osInfo = Get-CimInstance Win32_OperatingSystem
$hardwareInfo = Get-CimInstance Win32_ComputerSystem
@"
System Name: $($osInfo.CSName)
OS: $($osInfo.Caption), Version: $($osInfo.Version), Architecture: $($osInfo.OSArchitecture)
Manufacturer: $($hardwareInfo.Manufacturer), Model: $($hardwareInfo.Model), Memory: $($hardwareInfo.TotalPhysicalMemory/1GB) GB
Last Boot Time: $($osInfo.LastBootUpTime)
"@ | Out-File $file

# UAC Setting
$uacSetting = Get-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System' -Name EnableLUA
if ($uacSetting.EnableLUA -eq 0) {
    "Warning: UAC is disabled!" | Out-File $file -Append
} else {
    "UAC is enabled." | Out-File $file -Append
}

# Windows Firewall Status
$firewallProfile = Get-NetFirewallProfile | Where-Object { $_.Enabled -eq 'True' }
if ($firewallProfile) {
    "Windows Firewall is enabled for profiles: $($firewallProfile.Name -join ', ')" | Out-File $file -Append
} else {
    "Warning: Windows Firewall is disabled!" | Out-File $file -Append
}

# BitLocker Drive Encryption
$bitlockerStatus = Get-BitLockerVolume | Where-Object { $_.VolumeStatus -ne 'FullyDecrypted' }
if ($bitlockerStatus) {
    "$($bitlockerStatus.MountPoint) is encrypted using BitLocker." | Out-File $file -Append
} else {
    "Warning: No drives are encrypted using BitLocker!" | Out-File $file -Append
}

# Shared Folders
$sharedFolders = Get-SmbShare | Where-Object { $_.Name -ne 'IPC$' -and $_.Path }
if ($sharedFolders) {
    "Shared Folders: $($sharedFolders.Name -join ', ')" | Out-File $file -Append
}

# Local Administrators
$localAdmins = Get-LocalGroupMember -Group 'Administrators'
"Local Administrators: $($localAdmins.Name -join ', ')" | Out-File $file -Append

# Audit Failed Logins (Event ID 4625)
$failedLogins = Get-EventLog -LogName Security -InstanceId 4625 -Newest 10
if ($failedLogins) {
    "Last 10 Failed Login Attempts:" | Out-File $file -Append
    $failedLogins | ForEach-Object { "$($_.TimeGenerated) from $($_.ReplacementStrings[13])" } | Out-File $file -Append
}

# Check Autostart Locations in Registry
$autostartLocations = @(
    'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Run',
    'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce',
    'HKCU:\Software\Microsoft\Windows\CurrentVersion\Run',
    'HKCU:\Software\Microsoft\Windows\CurrentVersion\RunOnce'
)

"Autostart Locations in Registry:" | Out-File $file -Append
$autostartLocations | ForEach-Object {
    Get-ItemProperty -Path $_ | ForEach-Object {
        $_.PSObject.Properties | Where-Object { $_.Name -ne 'PSPath' -and $_.Name -ne 'PSParentPath' -and $_.Name -ne 'PSChildName' -and $_.Name -ne 'PSDrive' -and $_.Name -ne 'PSProvider' } | ForEach-Object {
            "$($_.Name) : $($_.Value)" | Out-File $file -Append
        }
    }
}

# Compliance checks

# Check for latest service packs and hotfixes from Microsoft
$latestHotfix = Get-HotFix | Sort-Object -Property InstalledOn -Descending | Select-Object -First 1
"Latest Installed Hotfix: $($latestHotfix.Description) - $($latestHotfix.InstalledOn)" | Out-File $file -Append

# Check minimum password length and complexity
$passwordPolicy = net accounts
if ($passwordPolicy -notlike "*Minimum password length:*" -or $passwordPolicy -notlike "*Password meets complexity requirements:*YES*") {
    "Recommendation: Set minimum password length and enable password complexity requirements." | Out-File $file -Append
}

# Output the results
$output | Out-File $file -Append
