#!/bin/bash

# Update and upgrade the system
apt update && apt upgrade -y

# Install necessary dependencies
apt install -y curl openssh-server ca-certificates tzdata perl

# Download and install the GitLab package (choose the version you prefer)
curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | bash
apt install -y gitlab-ce

# Configure GitLab
# Replace 'YOUR_SERVER_IP' with the actual IP address of your server
GITLAB_CONFIG="/etc/gitlab/gitlab.rb"
echo "external_url 'http://YOUR_SERVER_IP'" | tee -a $GITLAB_CONFIG

# Run the reconfigure command to apply changes
gitlab-ctl reconfigure

# Start GitLab services
gitlab-ctl start

# Enable GitLab to start on boot
systemctl enable gitlab-runsvdir

echo "GitLab installation is complete. Access it via 'http://YOUR_SERVER_IP'."
