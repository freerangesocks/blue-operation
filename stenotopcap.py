import datetime
import os
import subprocess

def convert_to_pcap(start, end, outdir, interval):
    # Ensure output directory exists
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    current_time = start
    while current_time < end:
        next_time = current_time + datetime.timedelta(seconds=interval)
        start_str = current_time.strftime('%Y-%m-%dT%H:%M:%SZ')
        end_str = next_time.strftime('%Y-%m-%dT%H:%M:%SZ')
        filename = f"{outdir}/{start_str}_{end_str}.pcap".replace(':', '')

        # Execute the stenoread command
        stenoread_command = f"/opt/stenographer/bin/stenoread after {start_str} and before {end_str} -w {filename}"
        subprocess.run(stenoread_command, shell=True)

        current_time = next_time

    print("Conversion Completed")

def get_datetime_input(prompt):
    while True:
        try:
            date_str = input(prompt)
            return datetime.datetime.strptime(date_str, '%B %d %Y')
        except ValueError:
            print("Invalid date format. Please use 'Month DD YYYY' (e.g., July 24 2018)")

def main():
    print("Stenographer File Conversion Tool")
    start_time = get_datetime_input("Enter start time (Month DD YYYY): ")
    end_time = get_datetime_input("Enter end time (Month DD YYYY): ")
    output_directory = input("Enter output directory: ")
    interval = int(input("Enter interval in seconds: "))

    convert_to_pcap(start_time, end_time, output_directory, interval)

if __name__ == "__main__":
    main()
