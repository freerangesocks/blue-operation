##############################################
#   Advanced_Windows_Audit_Tool_1.1.ps1      #
# This script should work on most windows    #
#            machines                        #
#  SOCKS created this 10/04/2023 with the    # 
# Intent of basic info gathering on windows  #
# Machines ... feel free to modify for use   #
##############################################
# Common Functions
function GatherSystemInfo {
    wmic computersystem get Caption,Manufacturer,Model,Name,NumberOfProcessors,TotalPhysicalMemory,SystemType,UserName
}

function NetworkInfo {
    wmic nicconfig where "IPEnabled='TRUE'" get Caption,DefaultIPGateway,Description,DHCPServer,IPAddress,IPSubnet,MACAddress
}

function MonitorProcesses {
    Get-Process | Format-Table Name, CPU, Id
}

function MonitorServices {
    Get-Service | Where-Object { $_.Status -eq "Running" } | Format-Table DisplayName, Status
}

function DiscoverLocalNetwork {
    # Get the IP address of the machine, considering both Ethernet and Wi-Fi connections
    $ipAddress = (Get-NetIPAddress -AddressFamily IPv4 | Where-Object { ($_.InterfaceAlias -notlike "*Loopback*") -and ($_.InterfaceAlias -like "Ethernet*" -or $_.InterfaceAlias -like "Wi-Fi*") -and $_.IPAddress -notlike "169.254.*" }).IPAddress
    
    if(-not $ipAddress) {
        Write-Warning "Could not determine IP address for network discovery."
        return
    }

    # Extract the base of the IP for CIDR purposes
    $baseIp = $ipAddress -replace "\d+$", ''

    # Generate IPs in the CIDR range
    $ips = 1..254 | ForEach-Object { "${baseIp}$_" }

    $aliveHosts = @()
    $totalCount = $ips.Count
    $currentIndex = 0

    foreach ($ip in $ips) {
        $currentIndex++

        # Progress bar
        $progressParams = @{
            Activity    = "Scanning network"
            Status      = "Checking $ip"
            PercentComplete = ($currentIndex / $totalCount) * 100
        }
        Write-Progress @progressParams

        $client = New-Object System.Net.Sockets.TcpClient
        try {
            $client.Connect($ip, 80)
            $aliveHosts += $ip
            $client.Close()
        } catch {
            # Ignore any errors (e.g., if the port is closed)
        }
    }
    Write-Progress @progressParams -Completed
    return $aliveHosts
}

function GetNetworkMap {
    $hosts = DiscoverLocalNetwork
    $arpTable = arp -a

    $networkMap = @()

    foreach ($host in $hosts) {
        $macAddress = ($arpTable | Where-Object { $_ -match $host }) -replace '.*((?:[a-fA-F0-9]{2}-){5}[a-fA-F0-9]{2}).*', '$1'
        $networkMap += [PSCustomObject]@{
            IP        = $host
            MAC       = $macAddress
        }
    }

    return $networkMap
}

# Security Check Functions
function ListFirewallRules {
    Get-NetFirewallRule | Where-Object { $_.Enabled -eq 'True' -and $_.Direction -eq 'Inbound' } | Format-Table Name, DisplayName, Action
}

function CheckLocalAdmins {
    net localgroup administrators
}

function CheckStartupLocations {
    Get-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Run', 
                     'HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce',
                     'HKCU:\Software\Microsoft\Windows\CurrentVersion\Run',
                     'HKCU:\Software\Microsoft\Windows\CurrentVersion\RunOnce' |
    Where-Object { $_.PSObject.Properties.Value } |
    Format-Table PSChildName, @{n='Value';e={$_.PSObject.Properties.Value}}
}

function CheckRemoteConnections {
    netstat -an | Where-Object { $_ -like '*ESTABLISHED*' } 
}

# Domain-specific Functions
function EnumerateDomainUsers {
    dsquery user
}

function EnumerateDomainAdmins {
    net group "Domain Admins" /domain
}

function EnumerateLockedOutAccounts {
    Search-ADAccount -LockedOut
}

function EnumerateDisabledAccounts {
    dsquery user -disabled
}

function UsersPasswordNeverExpire {
    Search-ADAccount -PasswordNeverExpires | Where-Object { $_.ObjectClass -eq 'user' } | Format-Table Name, ObjectClass, PasswordLastSet
}

# Check if the machine is domain-joined
function IsDomainJoined {
    $computerInfo = Get-WmiObject Win32_ComputerSystem
    return $computerInfo.PartOfDomain
}

# Main user prompt function
function UserPrompt {
    do {
        if (IsDomainJoined) {
            Write-Host " This is a Domain-Joined Endpoint Select Your Options:"
            Write-Host "1. Gather System Info"
            Write-Host "2. Network Info"
            Write-Host "3. Display Network Map (Experimental allow 20min)"
            Write-Host "4. Monitor Processes"
            Write-Host "5. Monitor Services"
            Write-Host "6. List Firewall Rules"
            Write-Host "7. Check Local Admins"
            Write-Host "8. Check Startup Locations"
            Write-Host "9. Check Remote Connections"
            Write-Host "10. Enumerate Domain Users"
            Write-Host "11. Enumerate Domain Admins"
            Write-Host "12. Enumerate Locked Out Accounts"
            Write-Host "13. Enumerate Disabled Accounts"
            Write-Host "14. Users Whose Passwords Never Expire"
            Write-Host "15. Exit"
        } else {
            Write-Host "This is a Standalone Endpoint Select Your Options:"
            Write-Host "1. Gather System Info"
            Write-Host "2. Network Info"
            Write-Host "3. Display Network Map (Experimental allow 60min)"
            Write-Host "4. Monitor Processes"
            Write-Host "5. Monitor Services"
            Write-Host "6. List Firewall Rules"
            Write-Host "7. Check Local Admins"
            Write-Host "8. Check Startup Locations"
            Write-Host "9. Check Remote Connections"
            Write-Host "10. Exit"
        }

        $choice = Read-Host "Enter your choice"
        
        switch($choice) {
            1 { GatherSystemInfo }
            2 { NetworkInfo }
            3 { GetNetworkMap }
            4 { MonitorProcesses }
            5 { MonitorServices }
            6 { ListFirewallRules }
            7 { CheckLocalAdmins }
            8 { CheckStartupLocations }
            9 { CheckRemoteConnections }
            10 {
                if (IsDomainJoined) {
                    EnumerateDomainUsers
                } else {
                    break
                }
            }
            11 { EnumerateDomainAdmins }
            12 { EnumerateLockedOutAccounts }
            13 { EnumerateDisabledAccounts }
            14 { UsersPasswordNeverExpire }
            15 { break }
        }
    } while ($true)
}

# Start the user prompt
UserPrompt
