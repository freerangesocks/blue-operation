#!/bin/bash

require_root() {
    if [[ $EUID -ne 0 ]]; then
        echo "You must run this script as root."
        exit 1
    fi
}

prompt_for_input() {
    echo "1) YUM-based (e.g., CentOS, RHEL)"
    echo "2) Debian-based (e.g., Ubuntu, Debian)"
    echo "3) Pacman-based (e.g., Arch, Manjaro)"
    read -p "Select OS (1/2/3): " os_choice

    echo "1) Server"
    echo "2) Endpoint"
    read -p "Is this a (1/2): " machine_choice

    echo "1) Standard Collection"
    echo "2) Incident Response Mode"
    read -p "Is this a (1/2): " mode_choice

    read -p "Enter the path where you want to save the information (or leave blank for current directory): " save_path
}

prepare_environment() {
    [[ -z "$save_path" ]] && save_path="./"
    save_path="${save_path%/}/" # Ensuring there's a trailing slash
    timestamp=$(date "+%Y-%m-%d_%H:%M:%S")
    output_file="${save_path}system_info_${timestamp}.txt"
}

write_header() {
    echo -e "\n=== $1 ===\n" >> "$output_file"
}

collect_data() {
    require_root
    prompt_for_input
    prepare_environment

    echo "Data collection started at: $timestamp" > "$output_file"

    # Hardware Details
    write_header "Hardware Details"
    lscpu >> "$output_file"
    lsmem >> "$output_file"
    lsblk >> "$output_file"
    lspci >> "$output_file"

    # Users & Authentication
    write_header "Users & Authentication"
    cat /etc/passwd >> "$output_file"
    cat /etc/group >> "$output_file"
    cat /etc/shadow >> "$output_file"
    who >> "$output_file"
    last >> "$output_file"
    echo "Passwordless Accounts:" >> "$output_file"
    awk -F: '($2 == "" ) { print $1 }' /etc/shadow >> "$output_file"

    # File System & Permissions
    write_header "File System & Permissions"
    cat /etc/fstab >> "$output_file"
    mount >> "$output_file"
    df -h >> "$output_file"
    du -sh /* >> "$output_file" 2>/dev/null

    # Processes & Services
    write_header "Processes & Services"
    ps aux >> "$output_file"
    systemctl list-unit-files --type=service >> "$output_file"
    netstat -tuln >> "$output_file" 2>/dev/null
    ss -tuln >> "$output_file" 2>/dev/null

    # Crontab entries
    write_header "Crontab Entries"
    for user in $(cut -f1 -d: /etc/passwd); do
        echo "Crontab for $user:" >> "$output_file"
        crontab -l -u $user >> "$output_file" 2>/dev/null
    done

    # Network Configuration
    write_header "Network Configuration"
    ip addr >> "$output_file"
    ip route >> "$output_file"
    iptables -L -vn >> "$output_file" 2>/dev/null

    # Software & Packages
    write_header "Software & Packages"
    case $os_choice in
        1) rpm -qa >> "$output_file";;
        2) dpkg -l >> "$output_file";;
        3) pacman -Q >> "$output_file";;
    esac

    # Logging & Monitoring
    write_header "Logging & Monitoring"
    cat /etc/logrotate.conf >> "$output_file"
    journalctl --since="$log_date" >> "$output_file" 2>/dev/null

    # Memory Dump in IR Mode
    if [[ $mode_choice -eq 2 ]]; then
        write_header "Memory Dump"
        dumpcap -w "${save_path}memory_dump_${timestamp}.pcap" && \
        echo "Memory dump saved to ${save_path}memory_dump_${timestamp}.pcap" >> "$output_file"
    fi

    # Kernel Parameters
    write_header "Kernel Parameters"
    sysctl -a | egrep "kern|boot|vm|fs|proc" >> "$output_file"

    echo "Data collected and saved to $output_file"
}

collect_data
