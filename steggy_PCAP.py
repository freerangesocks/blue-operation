import subprocess
import shlex
import os
from datetime import datetime, timedelta
import argparse

def run_command(command):
    process = subprocess.Popen(
        shlex.split(command),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    output, error = process.communicate()
    return process.returncode, output, error

def time_based_pcap(start, end, outdir, interval):
    # Convert start and end to timestamps
    start_ts = datetime.strptime(start, '%Y-%m-%dT%H:%M:%S')
    end_ts = datetime.strptime(end, '%Y-%m-%dT%H:%M:%S')

    # Create output directory if it doesn't exist
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    while start_ts < end_ts:
        next_ts = start_ts + timedelta(seconds=interval)
        if next_ts > end_ts:
            next_ts = end_ts

        start_str = start_ts.strftime('%Y-%m-%dT%H:%M:%S')
        end_str = next_ts.strftime('%Y-%m-%dT%H:%M:%S')
        
        filename = input(f"Enter the filename for the pcap covering {start_str} to {end_str}: ")
        if not filename.endswith('.pcap'):
            filename += '.pcap'
        
        filepath = os.path.join(outdir, filename)
        stenoread_command = f"/opt/stenographer/bin/stenoread after {start_str} and before {end_str} -w {filepath}"

        return_code, output, error = run_command(stenoread_command)
        if return_code != 0:
            print(f"Error creating pcap file: {error.decode().strip()}")
            break

        start_ts = next_ts

def query_based_pcap(query, tcpdump_args):
    stenocurl_command = f'stenocurl /query -d "{query}" --silent --max-time 890'
    tcpdump_command = f'tcpdump -r /dev/stdin -s 0 ' + ' '.join(tcpdump_args)

    # Execute the stenocurl command and pipe the output to tcpdump
    stenocurl_returncode, output, stenocurl_error = run_command(stenocurl_command)
    if stenocurl_returncode != 0:
        print(f"stenocurl error: {stenocurl_error.decode().strip()}")
        return

    tcpdump_returncode, pcap_output, tcpdump_error = run_command(tcpdump_command)
    if tcpdump_returncode != 0:
        print(f"tcpdump error: {tcpdump_error.decode().strip()}")
        return

    filename = input("Enter the filename to save the pcap output: ")
    if not filename.endswith('.pcap'):
        filename += '.pcap'
    
    with open(filename, 'wb') as f:
        f.write(pcap_output)
    print(f"Output saved to {filename}")

def main():
    parser = argparse.ArgumentParser(description='Network Traffic Capture Tool')
    parser.add_argument('-m', '--mode', type=str, choices=['time-based', 'query-based'], 
                        help="Mode of operation: 'time-based' or 'query-based'")
    args, unknown = parser.parse_known_args()

    if args.mode == 'time-based':
        start = input("Enter the start time (YYYY-MM-DDTHH:MM:SS): ")
        end = input("Enter the end time (YYYY-MM-DDTHH:MM:SS): ")
        outdir = input("Enter the output directory path: ")
        interval = int(input("Enter the seconds per pcap interval: "))
        time_based_pcap(start, end, outdir, interval)

    elif args.mode == 'query-based':
        query = input("Enter the Stenographer query: ")
        tcpdump_args_str = input("Enter any additional tcpdump arguments: ")
        tcpdump_args = shlex.split(tcpdump_args_str)
        query_based_pcap(query, tcpdump_args)

    else:
        print("Please specify the mode of operation using -m or --mode.")

if __name__ == '__main__':
    main()
